"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# These tables contain a pairing of a distance and a speed from the brevet control table.
# The distances are additive, that is, the second distance in the MINIMUM is from 601 to 1000, not 1 to 400
MINIMUM_SPEED_PER_DIST = [(600,15),(400,11.428),(300,13.333)]
MAXIMUM_SPEED_PER_DIST = [(200,34),(200,32),(200,30),(400,28),(300,26)]

def open_time(control_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    openTime = arrow.get(brevet_start_time)    # Copy the input time so we can edit it then output it
    remainingDist = control_dist_km # Copy the control distance so we can use it to count
    hoursToAdd = 0                  # We'll need this and the next variable later to implement the acp control formula
    minsToAdd = 0
    for DIST_SPEED in MAXIMUM_SPEED_PER_DIST:  # This is a loop that iterates through the brevet control table and calculates the time
        distance, speed = DIST_SPEED           # Each entry in the table has an accepted speed for a certain distance covered
        if (remainingDist >= distance):        #| For each entry in the table, if the remaining distance is greater than the distance 
            hoursToAdd += (distance / speed)   #| entry in the table, that means we can just use the whole row of the table as is:
            remainingDist -= distance          #| that is, we divide that distance by the speed, and add it to our running total
        elif (remainingDist > 0):                   #| If there's still remaining distance but it's less than the distance entry in the,
            hoursToAdd += (remainingDist / speed)   #| we just apply the formula: divide the distance by the speed and add it to the total
            remainingDist = 0                       #| then we set the remining dist to zero because we just used up our remainder
        else:        # If the remaining distance was 0 or less then there's no distance left to find speed over, so break the loop
            break
    minsToAdd = hoursToAdd % 1   # The minutes from the formula come from the decimal portion of the running total
    hoursToAdd = hoursToAdd // 1 # The hours to add to the time are the whole number of the running total
    minsToAdd *= 60  # Then to figure out the minutes from the decimal portion we just multiple by 60
    hoursToAdd = int(round(hoursToAdd)) # Round the numbers out so we dont add fractions to the clock
    minsToAdd = int(round(minsToAdd))
    openTime = openTime.shift(hours=hoursToAdd, minutes=minsToAdd) # Then increase the entered time by that much time and return it
    return openTime.isoformat()

# This next function is exactly the same as the previous, except with a slight variation if the control distance is 60 or less,
# so I wil only be commenting on those changes.
def close_time(control_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    closeTime = arrow.get(brevet_start_time)
    remainingDist = control_dist_km
    hoursToAdd = 0
    minsToAdd = 0
    if (remainingDist <= 60):               # This is the control distance 60 or less case,
        hoursToAdd += 1                     # the formula says add 1 hour and then use the formula as you would,
        hoursToAdd += (remainingDist / 20)  # except with a speed of 20 kmph
    else:
        for DIST_SPEED in MINIMUM_SPEED_PER_DIST:
            distance, speed = DIST_SPEED
            if (remainingDist >= distance):
                hoursToAdd += (distance / speed)
                remainingDist -= distance
            elif (remainingDist > 0):
                hoursToAdd += (remainingDist / speed)
                remainingDist = 0
            else:
                break
    minsToAdd = hoursToAdd % 1
    hoursToAdd = hoursToAdd // 1
    minsToAdd *= 60
    hoursToAdd = int(round(hoursToAdd))
    minsToAdd = int(round(minsToAdd))
    closeTime = closeTime.shift(hours=hoursToAdd, minutes=minsToAdd)
    return closeTime.isoformat()
