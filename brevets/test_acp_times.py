"""
This is a nose test suite for the acp_times module
"""
import arrow
import nose
import acp_times

zeroTime = arrow.get(0)

# This will put a variety of different distances into the acp control point open time calculator and compare it to the 
# result generated from that distance on the website
def test_open_time():
    assert acp_times.open_time(0,zeroTime) == zeroTime.shift(minutes=0).isoformat()
    assert acp_times.open_time(23,zeroTime) == zeroTime.shift(minutes=41).isoformat()
    assert acp_times.open_time(51,zeroTime) == zeroTime.shift(hours=1,minutes=30).isoformat()
    assert acp_times.open_time(98,zeroTime) == zeroTime.shift(hours=2,minutes=53).isoformat()
    assert acp_times.open_time(173,zeroTime) == zeroTime.shift(hours=5,minutes=5).isoformat()
    assert acp_times.open_time(248,zeroTime) == zeroTime.shift(hours=7,minutes=23).isoformat()
    assert acp_times.open_time(398,zeroTime) == zeroTime.shift(hours=12,minutes=4).isoformat()
    assert acp_times.open_time(405,zeroTime) == zeroTime.shift(hours=12,minutes=18).isoformat()
    assert acp_times.open_time(560,zeroTime) == zeroTime.shift(hours=17,minutes=28).isoformat()
    assert acp_times.open_time(622,zeroTime) == zeroTime.shift(hours=19,minutes=35).isoformat()
    assert acp_times.open_time(718,zeroTime) == zeroTime.shift(hours=23,minutes=1).isoformat()
    assert acp_times.open_time(869,zeroTime) == zeroTime.shift(days=1,hours=4,minutes=24).isoformat()
    assert acp_times.open_time(901,zeroTime) == zeroTime.shift(days=1,hours=5,minutes=33).isoformat()
    assert acp_times.open_time(1000,zeroTime) == zeroTime.shift(days=1,hours=9,minutes=5).isoformat()

# This will put a variety of different distances into the acp control point close time calculator and compare it to the 
# result generated from that distance on the website
def test_close_time():
    assert acp_times.close_time(0,zeroTime) == zeroTime.shift(hours=1).isoformat()
    assert acp_times.close_time(23,zeroTime) == zeroTime.shift(hours=2,minutes=9).isoformat()
    assert acp_times.close_time(51,zeroTime) == zeroTime.shift(hours=3,minutes=33).isoformat()
    assert acp_times.close_time(98,zeroTime) == zeroTime.shift(hours=6,minutes=32).isoformat()
    assert acp_times.close_time(173,zeroTime) == zeroTime.shift(hours=11,minutes=32).isoformat()
    assert acp_times.close_time(248,zeroTime) == zeroTime.shift(hours=16,minutes=32).isoformat()
    assert acp_times.close_time(398,zeroTime) == zeroTime.shift(days=1,hours=2,minutes=32).isoformat()
    assert acp_times.close_time(405,zeroTime) == zeroTime.shift(days=1,hours=3).isoformat()
    assert acp_times.close_time(560,zeroTime) == zeroTime.shift(days=1,hours=13,minutes=20).isoformat()
    assert acp_times.close_time(622,zeroTime) == zeroTime.shift(days=1,hours=17,minutes=56).isoformat()
    assert acp_times.close_time(718,zeroTime) == zeroTime.shift(days=2,hours=2,minutes=20).isoformat()
    assert acp_times.close_time(869,zeroTime) == zeroTime.shift(days=2,hours=15,minutes=32).isoformat()
    assert acp_times.close_time(901,zeroTime) == zeroTime.shift(days=2,hours=18,minutes=20).isoformat()
    assert acp_times.close_time(1000,zeroTime) == zeroTime.shift(days=3,hours=3).isoformat()
